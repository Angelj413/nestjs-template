import { Module } from "@nestjs/common";

import { AppController } from "./app/app.controller";

@Module({
  imports: [AppController],
  providers: [],
  exports: [],
})
export class DomainModule {}
