import { Injectable } from "@nestjs/common";
import { configure, Logger } from "log4js";
const { Logging } = require("@google-cloud/logging");
import morgan from "morgan";

export interface ILogLine {
  time: string;
  level: string;
  message: string;
}

@Injectable()
export class Log4jsLogger {
  public defaultLogger: Logger;
  public expressLogger: Logger;
  public log: any;
  private level: string;

  constructor() {
    const loggings = new Logging();
    this.log = loggings.log("logging");
    const logConfig = configure({
      appenders: {
        console: { type: "console" },
      },
      categories: {
        default: { appenders: ["console"], level: "debug" },
        express: { appenders: ["console"], level: "debug" },
      },
    });
    this.defaultLogger = logConfig.getLogger("default");
    this.expressLogger = logConfig.getLogger("express");
  }

  public trace = (message: any, message_gcp: any) => {
    this.defaultLogger.trace(message);
  };

  public debug = (message: any, message_gcp: any) => {
    this.defaultLogger.debug(message);

    const metadata = {
      resource: {
        type: "gce_instance",
      },
      severity: "DEBUG",
    };

    const entry = this.log.entry(metadata, message_gcp);

    this.log
      .write(entry)
      .then(() => {})
      .catch((err) => {});
  };

  public info = (message: any, message_gcp: any) => {
    this.defaultLogger.info(message);
    const metadata = {
      resource: {
        type: "gce_instance",
      },
      severity: "INFO",
    };

    const entry = this.log.entry(metadata, message_gcp);

    this.log
      .write(entry)
      .then(() => {})
      .catch((err) => {});
  };

  public warn = (message: any, message_gcp: any) => {
    this.defaultLogger.warn(message);

    const metadata = {
      resource: {
        type: "gce_instance",
      },
      severity: "WARNING",
    };

    const entry = this.log.entry(metadata, message_gcp);

    this.log
      .write(entry)
      .then(() => {})
      .catch((err) => {});
  };

  public error = (message: any, message_gcp: any) => {
    this.defaultLogger.error(message);

    const metadata = {
      resource: {
        type: "gce_instance",
      },
      severity: "ERROR",
    };

    const entry = this.log.entry(metadata, message_gcp);

    this.log
      .write(entry)
      .then(() => {})
      .catch((err) => {});
  };

  public fatal = (message: any, message_gcp: any) => {
    this.defaultLogger.fatal(message);
  };

  public getExpressLogger = () => {
    return morgan(
      ":method :url HTTP/:http-version | :status | :response-time ms | :res[content-length] | :remote-addr | :remote-user",
      {
        stream: {
          write: this.logStream,
        },
      }
    );
  };

  public logStream = (message: string) => {
    const statusCode = Number(message.split(" | ")[1]);
    if (statusCode < 400) {
      this.expressLogger.info(message.trim());
    } else {
      this.expressLogger.error(message.trim());
    }
  };
}
