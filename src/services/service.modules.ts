import { Module } from '@nestjs/common';
import { LoggerModule } from '@services/logger/logger.module';

@Module({
  imports: [],
  providers: [LoggerModule],
  exports: [LoggerModule],
})
export class ServicesModule {}
