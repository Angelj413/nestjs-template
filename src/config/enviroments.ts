export const enviroments = {
  local: '.local.env',
  dev: '.dev.env',
  stag: '.stag.env',
  prod: '.prod.env',
};
  