export default () => ({
  PORT: parseInt(process.env.PORT, 10),
  ROOT_PATH: process.env.ROOT_PATH,
  NODE_ENV: process.env.NODE_ENV,
  LOGGER_LEVEL: process.env.LOGGER_LEVEL,
  TZ: process.env.TZ
});
