import { NestFactory } from "@nestjs/core";
import { ValidationPipe } from "@nestjs/common";
import { NestExpressApplication } from "@nestjs/platform-express";
import { AppModule } from "./app.module";
import { Log4jsLogger } from "@services/logger/loggerLog4js/loggerLog4js.service";
import { ConfigService } from "@nestjs/config";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import helmet from 'helmet';

async function bootstrap() {
  const logger = new Log4jsLogger();
  const expressLogger = logger.getExpressLogger();

  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  const configService = app.get(ConfigService);
  const ROOT_PATH = configService.get("ROOT_PATH");
  const PORT = configService.get("PORT");

  app.setGlobalPrefix(ROOT_PATH);
  
  const config = new DocumentBuilder()
    .setTitle("Backend Template")
    .setDescription("Proyecto template backend")
    .setVersion("1.0")
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(`${ROOT_PATH}/docs`, app, document);

  app.useGlobalPipes(new ValidationPipe());
  app.enableCors({ origin: "*" });
  app.use(expressLogger);
  app.use(helmet());

  await app.listen(PORT);
}
bootstrap();
