import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import configuration from "./config/configuration";
import { DomainModule } from "./domains/domains.module";
import { ServicesModule } from "./services/service.modules";
import * as Joi from "joi";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `.env.${process.env.NODE_ENV}`,
      load: [configuration],
      validationSchema: Joi.object({
        NODE_ENV: Joi.string()
          .valid("development", "production")
          .default("development"),
        ROOT_PATH: Joi.string().required(),
        PORT: Joi.number().default(80).optional(),
        LOGGER_LEVEL: Joi.string().required(),
        TZ: Joi.string().default("America/Mexico_City"),
      }),
      validationOptions: {
        allowUnknown: true,
        abortEarly: true,
      },
    }),
    DomainModule,
    ServicesModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
