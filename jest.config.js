module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    testPathIgnorePatterns: ['/node_modules/', '/dist/'],
    collectCoverage: true,
    collectCoverageFrom: [
      'src/**/*.{ts,tsx,js}',
      '!src/index.ts',
      '!src/@service.modules.ts/**',
    ],
    coverageDirectory: 'test/coverage',
    moduleNameMapper: {
      '^~(.*)$': '<rootDir>/src$1',
      '^@config(.*)$': '<rootDir>/src/services/logger$1',
    },
    coverageThreshold: {
      global: {
        branches: 0,
        functions: 0,
        lines: 0,
        statements: 0,
      },
    },
  }
  